# Comic Book Application

_Web application that consumes the restful API of Comic Vine which is the largest comic database online._

## Folders
- Comic
- Server

### Comic
_This folder contains the web application developed in react which shows the latest issues and the latest volumes of the comicvine API._ 

### server
_This folder contains the server to run the client._



## Requirements 📋


*  Nodejs

*  npm


## Download 📥 

_To download this project you must follow the following steps:_
    
```
git clone 'https://gitlab.com/willked/comics.git'
```

*  Copy the url of the clone button to the repository.

*  On your local console run the command


## Start 🚀

* On the server folder open a terminal and run the command

```
npm install
```

```
npm start
```

* On the comic folder open a terminal and run the command

```
npm install
```

```
npm start
```



Thanks a lot. 👍