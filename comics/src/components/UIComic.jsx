import styles from "./Comic.module.css";
import {Container, Row, Col, Image, Spinner} from 'react-bootstrap'
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ReactHtmlParser from 'react-html-parser';

// componente que muestra un solo comic
export function UIComic(){
    const { comicId } = useParams();
    const [comic, setComic] = useState(null);

    const url = "http://localhost:5000/api/volumes/?api_key=5a21ed99484e356cfff3673b4b0664e231db5d78&filter=id:"+comicId+"&format=json";
    const [errAPI, seterrAPI] = useState([]);
    
    const fetchApi = async () => {
        try {
            const response = await fetch(url)
            const responseJSON = await response.json()
            setComic(responseJSON.results);
            seterrAPI(false)
        } catch (e) {
            seterrAPI(true);
        }
    }

    useEffect( () => {
        fetchApi();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [comicId])

    if(errAPI === false){
        let publisher = "";
        if( typeof comic[0].publisher === 'object' ){
            publisher = comic[0].publisher.name;
        }
        else{
            publisher = comic[0].publisher;
        }
        return (    
            <Container>
                <Row className="text-center">
                    <Col xs>
                        <h1 className={styles.title}><b>{comic[0].name}</b></h1><br />
                    </Col>
                </Row>
                <Row>
                    <Col xs="12" md="4">
                        <Image src={comic[0].image.medium_url} alt={comic[0].name} thumbnail />
                    </Col>
                    <Col xs="12" md="8">
                        <br />
                        {ReactHtmlParser(comic[0].description)}
                        <p>{comic[0].deck}</p>
                        <p><b className={styles.title}>Publisher by: </b> {publisher}</p>
                        <p><b className={styles.title}>year: </b> {comic[0].start_year}</p>                    
                    </Col>
                </Row>
            </Container>
        );
    }
    // Existen errores al consumir la API 
    else if(errAPI === true){
        return (
            <Container className={styles.back}>
                <Row className="text-center">
                    <Col xs>
                        <h1>Houston, we have a problem!</h1><br />
                        <h3>Check your internet connection and try again!</h3>
                    </Col>
                </Row>
            </Container>
        );
    }
    // La variable es vacia, está haciendo la busqueda 
    else{
        return (
            <div className="text-center">
                <br />
                <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
                </Spinner> Loading..
            </div>
        );
    }
}