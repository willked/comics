import { useEffect, useState } from "react";
import { Container, Row, Col, Spinner } from "react-bootstrap";
import { Link } from "react-router-dom";
import styles from "./ComicsGird.module.css";


export function ComicsGird(){   

    const url = "http://localhost:5000/api/volumes/?api_key=5a21ed99484e356cfff3673b4b0664e231db5d78&sort=date_added:desc&format=json";
    const [comics, setComics] = useState([]);
    const [errAPI, seterrAPI] = useState([]);

    const fetchApi = async () => {
        try {
            const response = await fetch(url)
            const responseJSON = await response.json()
            setComics(responseJSON.results);
            seterrAPI(false)
        } catch (e) {
            seterrAPI(true);
        }
    }

    useEffect( () => {
        fetchApi()
    }, [])

    
    // No existen errores al consumir la API 
    if(errAPI === false){
        return (               
            <ul className={styles.gird}>            
                {comics.map( (comic) => {
                    return (
                    <li className={styles.card} key={comic.id} >
                        <Link to={"/volume/"+comic.id}>
                            <img className={styles.cardImage} src={comic.image.medium_url} alt={comic.name} 
                            height={200} width={146} />
                            <div className={styles.letters}>
                                {comic.name}
                                <br />
                                {comic.start_year}
                            </div>
                        </Link>
                    </li>)
                })}
            </ul>
        );
    } 
    // Existen errores al consumir la API 
    else if(errAPI === true){
        return (
            <Container className={styles.back}>
                <Row className="text-center">
                    <Col xs>
                        <h1>Houston, we have a problem!</h1><br />
                        <h3>Check your internet connection and try again!</h3>
                    </Col>
                </Row>
            </Container>
        )
    }
    // La variable es vacia, está haciendo la busqueda 
    else{
        return (
            <div className="text-center">
                <br />
                <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
                </Spinner> Loading..
            </div>
        )
    }
    
}