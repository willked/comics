import { Button, ButtonGroup } from "react-bootstrap";
import { CarouselElem } from "./CarouselElem";
import { UIIssues } from "./UIIssues";

export function UIHome(){
    return(
        <div>
            <CarouselElem />
            <div className="text-center">
                <h1>ISSUES</h1>
            </div>
            <UIIssues />
        </div>
    )
}