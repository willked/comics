import Carousel from 'react-bootstrap/Carousel'
import styles from "./CarouselElem.module.css"

// funcion para ejecutar el carousel de bootstrap-react
export function CarouselElem(){
    return (
        <Carousel fade>        
            <Carousel.Item interval={4000} className={styles.papa}>
                <img className={"d-block w-100 "+styles.degrade} src={process.env.PUBLIC_URL+"carousel1.jpg"} alt="Imagen" height={400}  />                
                <Carousel.Caption>
                    <h3>Epic sentence</h3>
                    <p>"It is decisions that make us who we are, and we can always choose to do the right thing".<br />Spiderman</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
                <img className={"d-block w-100 "+styles.degrade} src={process.env.PUBLIC_URL+"carousel2.jpg"} alt="Imagen" height={400}  />                
                <Carousel.Caption>
                    <h3>Epic sentence</h3>
                    <p>"Absolute power corrupts absolutely"<br />Charles Xavier</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
                <img className={"d-block w-100 "+styles.degrade} src={process.env.PUBLIC_URL+"carousel3.jpg"} alt="Imagen" height={400}  />                
                <Carousel.Caption>
                    <h3>Did you know?</h3>
                    <p>The X-Men No.1 has been the best-selling comic in Marvel Comics history. It is estimated that up to 8 million copies were distributed!</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
                <img className={"d-block w-100 "+styles.degrade} src={process.env.PUBLIC_URL+"carousel4.jpg"} alt="Imagen" height={400}  />                
                <Carousel.Caption>
                    <h3>Epic sentence</h3>
                    <p>"If you think Arkham is scary as a doctor, you should try it as a patient"<br />Harley Quinn</p>                    
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
                <img className={"d-block w-100 "+styles.degrade} src={process.env.PUBLIC_URL+"carousel5.jpg"} alt="Imagen" height={400}  />                
                <Carousel.Caption>
                    <h3>Curiosity</h3>
                    <p>Marvel's oldest superhero is the Human Torch, which appeared in the first issue they released.</p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>)
    
}

