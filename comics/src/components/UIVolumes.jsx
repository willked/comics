
import { CarouselElem } from "./CarouselElem";
import { ComicsGird } from "./ComicsGird";

export function UIVolumes(){
    return(
        <div>
            <CarouselElem />
            <div className="text-center">
                <h1>VOLUMES</h1>
            </div>
            <ComicsGird />
        </div>
    )
}