import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {
    BrowserRouter as Router,
    Switch,
    Link,
    Route
  } from "react-router-dom";
import { UIComic } from './components/UIComic';
import { UIComicIssue } from './components/UIComicIssue';
import { UIHome } from './components/UIHome';
import { UIVolumes } from './components/UIVolumes';

export function App(){
    return (<div>
        <Router>
            <header>
                <Navbar expand="lg" bg="dark" variant="dark">
                    <Navbar.Brand as={Link} to="/"><h1>ALL COMICS</h1></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                        </Nav>
                        <Nav>
                            <Nav.Link as={Link} to="/">Issues</Nav.Link>
                            <Nav.Link as={Link} to="/volumes">Volumes</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>          
            </header>
            <main>                
                <Switch>
                    <Route path="/comic/:comicId">
                        <UIComicIssue />                        
                    </Route>
                    <Route path="/volume/:comicId">
                        <UIComic />                        
                    </Route>
                    <Route path="/volumes">
                        <UIVolumes />                    
                    </Route>                   
                    <Route path="/">
                        <UIHome />                        
                    </Route>
                </Switch>
            </main>
            </Router>
    </div>)
}